# README
Installing this extension pack should install all recommended extensions for developing for TPLove.

The extensions are as follows:

## TPLove specific
* [TPLove Snippets](https://marketplace.visualstudio.com/items?itemName=tapps.tplove-snippets)
## Lua extensions
* [Lua for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=trixnz.vscode-lua)
