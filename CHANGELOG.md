# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2018-02-16
- Remove dwenegar.vscode-luacheck as dependency

## [1.0.1] - 2018-02-01
- Improve README

## [1.0.0] - 2018-02-01
- Initial release
